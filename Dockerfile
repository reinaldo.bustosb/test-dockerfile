FROM node:latest

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

RUN git --version

RUN git clone https://gitlab.com/reinaldo.bustosb/my_app

WORKDIR /usr/src/app/my_app

RUN npm install 

EXPOSE 3000

CMD [ "npm", "start" ]